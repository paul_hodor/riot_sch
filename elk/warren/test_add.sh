#!/bin/bash

curl -X POST "localhost:9200/warren/test/" -H 'Content-Type: application/json' -d'
{
  "title": "Title test1",
  "text":  "Text test1",
  "date":  "2014/01/01"
}
'

curl -X POST "localhost:9200/warren/test/111" -H 'Content-Type: application/json' -d'
{
  "title": "Title test2",
  "text":  "Text test2",
  "date":  "2014/01/01"
}
'

curl -X POST "localhost:9200/warren/test/" -H 'Content-Type: application/json' -d'
{
  "title": "Title test3",
  "text":  "Text test3...",
  "date":  "2014/01/01"
}
'
