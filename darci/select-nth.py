#!/usr/bin/env python

# Read lines from standard input and output every nth line to standard output

from docopt import docopt
import time
import sys

doc = """

Read lines from standard input and write every nth line to standard output

Usage:
  select-nth.py -n <num>

Options:
  -n <num>   Multiples of this line number will be kept in the output
  -h --help  Show this screen
"""

opts = docopt(doc)
print(opts)
print("")

n = int(opts["-n"])
l = 0
for line in sys.stdin:
  l = l+1
  if l % n == 0:
    sys.stdout.write(line)
    sys.stdout.flush()
    l = 0
