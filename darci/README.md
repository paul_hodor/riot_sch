This directory contains files associated with running the DARCI device from
a Ubuntu virtual machine.

* ```darci_01.js```    - Script to pull data from DARCI
* ```sample.data```    - 100 lines of output from running the darci_01.js script
* ```select-nth.py```  - Python 3 script that subsamples the data. It takes an
                         argument that specifies the count of lines in the input
                         for which a single line is sent to the output

The virtual machine needs to be set up on Virtualbox on a Windows computer.
The DARCI driver exists only for Windows. Follow Cary's instructions in file
"```darci-v121_api-nodejs_2018a.pdf```". After finishing the instructions, also
install the mosquitto clients per instructions on the mosquitto website.

To run the Python script, a Python 3 virtual environment needs to exist that
has the module "docopt" isntalled. The virtual environment is created like
this:

```
sudo apt install python3-venv
python3 -m venv ~/workenv
source ~/workenv/bin/activate
pip install --upgrade pip
pip install docopt
deactivate
```

To run DARCI and submit all data to an MQTT broker, run:

```
sudo node darci_01.js | mosquitto_p -t "darci/1" -h 35.163.202.37 -l
```

To run DARCI with subsampling at 5 Hz:

```
source ~/workenv/bin/activate
sudo node darci_01.js | python select-nth.py -n 20 | mosquitto_p -t "darci/1" -h 35.163.202.37 -l
```

