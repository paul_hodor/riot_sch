function ReadSerialData(data) {
  console.log(data);
  //port.write('4\n')  //requests another darci measurement
  port.write('5\n');
}

const SerialPort = require('serialport');
const port = new SerialPort('/dev/ttyACM0', {baudRate: 115200});
//port.write('4\n') //request initial darci measurement
port.write('5\n'); //requests darci status

const parsers = SerialPort.parsers;
const parser = new parsers.Readline({ delimiter: '\n'});
port.pipe(parser);
parser.once('data', ReadSerialData); //all requests  except '4\n'
parser.on('data', ReadSerialData); // only the '4\n' request


