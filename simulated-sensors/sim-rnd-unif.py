#!/usr/bin/env python

# Output random numbers following a uniform distribution

from docopt import docopt
import time
import sys
import random

doc="""

Output simulated sensor data as uniformly distributed random numbers

One data point per line

Usage:
  sim-rnd-unif.py [-i <id> -s <sep> -t <time-step> -n <min> -x <max> -d <seed>  -p] 

Options:
  -i <id>         Identifier for this sensor given as a string [default: ]
  -s <sep>        Separator string used in the output [default: \t]
  -t <time-step>  Time interval in seconds between consecutive data points [default: 0.1]
  -n <min>        Minimum float value of data range [default: 0]
  -x <max>        Maximum float value of data range [default: 1]
  -d <seed>       An integer to initialize the random number generator
  -p              Append time stamp
  -h --help       Show this screen.
"""

# Parse the command line based on the docopts string specification, load them
# into dictionary "opts"
opts = docopt(doc)
#print("Command line options:")
#print(opts)
#print("")

# load the command-line options into variables
sid = opts["-i"]
sep = opts["-s"]
step = float(opts["-t"])
rmin = float(opts["-n"])
rmax = float(opts["-x"])
seed = (opts["-d"])
if seed is not None:
    seed = int(opts["-d"])
timestamp = opts["-p"]

random.seed(seed)
while True:
    msg = ""
    if sid != "":
        msg = f'{sid}{sep}'
    msg = msg + f'{random.uniform(rmin, rmax):.4f}'
    if timestamp:
        msg = msg + f'{sep}{time.time():.3f}'
    print(msg)
    sys.stdout.flush()
    time.sleep(step)
    
