#!/usr/bin/env python

# Output a sinus wave pattern

from docopt import docopt
import time
import sys
import math

doc="""

Output simulated sensor data in a saw-tooth pattern

One data point per line

Usage:
  sim-sin.py [-i <id> -s <sep> -t <time-step> -o <period> -a <amplitude>
      -f <phase> -p] 

Options:
  -i <id>         Identifier for this sensor given as a string [default: ]
  -s <sep>        Separator string used in the output [default: \t]
  -t <time-step>  Time interval in seconds between consecutive data points [default: 0.1]
  -o <period>     Number of consecutive data points that make a 2 pi period [default: 100]
  -a <amplitude>  Factor by which the sinus function gets multiplied [default: 1]
  -f <phase>      Additive term to the argument of sinus [default: 0]
  -p              Append time stamp
  -h --help       Show this screen.
"""

# Parse the command line based on the docopts string specification, load them
# into dictionary "opts"
opts = docopt(doc)
#print("Command line options:")
#print(opts)
#print("")

# load the command-line options into variables
sid = opts["-i"]
sep = opts["-s"]
step = float(opts["-t"])
period = float(opts["-o"])
amplitude = float(opts["-a"])
phase = float(opts["-f"])
timestamp = opts["-p"]

x = 0.0
while True:
    msg = ""
    if sid != "":
        msg = f'{sid}{sep}'
    msg = msg + f'{amplitude * math.sin(2*math.pi*x/period + phase):.4f}'
    if timestamp:
        msg = msg + f'{sep}{time.time():.3f}'
    print(msg)
    sys.stdout.flush()
    x = x + 1.0
    if x > period:
        x = x - period
    time.sleep(step)
