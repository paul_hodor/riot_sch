#!/usr/bin/env python

# Output a saw-tooth wave pattern

from docopt import docopt
import time
import sys

doc="""

Output simulated sensor data in a saw-tooth pattern

One data point per line

Usage:
  sim-saw.py [-i <id> -s <sep> -t <time-step> -n <steps> -m <max> -p] 

Options:
  -i <id>         Identifier for this sensor given as a string [default: ]
  -s <sep>        Separator string used in the output [default: \t]
  -t <time-step>  Time interval in seconds between consecutive data points [default: 0.1]
  -n <steps>      Number of datapoints in a tooth [default: 20]
  -m <max>        Maximum of data range, minumum is 0 [default: 19]
  -p              Append time stamp
  -h --help       Show this screen.
"""

# Parse the command line based on the docopts string specification, load them
# into dictionary "opts"
opts = docopt(doc)
#print("Command line options:")
#print(opts)
#print("")

# load the command-line options into variables
sid = opts["-i"]
sep = opts["-s"]
step = float(opts["-t"])
n = int(opts["-n"])
smax = float(opts["-m"])
timestamp = opts["-p"]

# differences smaller than this value are considered to be 0, to deal with
# rounding errors
err = 0.00001 * smax

delta = (smax + 1)/n
x = 0
while True:
    msg = ""
    if sid != "":
        msg = f'{sid}{sep}'
    msg = msg + f'{x:.1f}'
    if timestamp:
        msg = msg + f'{sep}{time.time():.3f}'
    print(msg)
    sys.stdout.flush()
    x = x + delta
    if x > smax + err:
        x = 0
    time.sleep(step)
