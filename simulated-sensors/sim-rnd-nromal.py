#!/usr/bin/env python

# Output random numbers following a normal distribution

from docopt import docopt
import time
import sys
import random

doc="""

Output simulated sensor data as normally distributed random numbers

One data point per line

Usage:
  sim-rnd-normal.py [-i <id> -s <sep> -t <time-step> -m <mean> -v <stdev> -d <seed>  -p] 

Options:
  -i <id>         Identifier for this sensor given as a string [default: ]
  -s <sep>        Separator string used in the output [default: \t]
  -t <time-step>  Time interval in seconds between consecutive data points [default: 0.1]
  -m <mean>       The mean of the normal distribution [default: 0]
  -v <stdev>      The standard deviation of the normal distribution [default: 1]
  -d <seed>       An integer to initialize the random number generator
  -p              Append time stamp
  -h --help       Show this screen.
"""

# Parse the command line based on the docopts string specification, load them
# into dictionary "opts"
opts = docopt(doc)
#print("Command line options:")
#print(opts)
#print("")

# load the command-line options into variables
sid = opts["-i"]
sep = opts["-s"]
step = float(opts["-t"])
mean = float(opts["-m"])
stdev = float(opts["-v"])
seed = (opts["-d"])
if seed is not None:
    seed = int(opts["-d"])
timestamp = opts["-p"]

random.seed(seed)
while True:
    msg = ""
    if sid != "":
        msg = f'{sid}{sep}'
    msg = msg + f'{random.gauss(mean, stdev):.4f}'
    if timestamp:
        msg = msg + f'{sep}{time.time():.3f}'
    print(msg)
    sys.stdout.flush()
    time.sleep(step)
    
